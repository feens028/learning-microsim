---
title: "Progress towards SDG 1 and 10 reduced throughout all regions in Ethiopa as a consequence of COVID-19 - Supplementary Information"
author:
  - Michiel van Dijk:
      email: michiel.vandijk@wur.nl
      institute: [WUR, IIASA]
      correspondence: true
institute:
  - WUR: Wageningen Economic Research, the Hague, the Netherlands
  - IIASA: International Institute for Applied Systems Analysis, Laxenburg, Austria
date: "`r format(Sys.time(), '%B %d, %Y')`"
output:
 bookdown::pdf_document2:
  citation_package: biblatex
  keep_tex: true
  toc: true
  number_sections: true
  dev: png
  pandoc_args:
      - '--lua-filter=scholarly-metadata.lua'
      - '--lua-filter=author-info-blocks.lua'
bookdown::word_document2:
  reference_docx: word_style.docx
  pandoc_args:
      - '--lua-filter=scholarly-metadata.lua'
      - '--lua-filter=author-info-blocks.lua'
bibliography: gfsp-review.bib
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding,
  output_format = "bookdown::pdf_document2")})
header-includes: 
  \usepackage[backend=biber, useprefix=true, style=apa, citestyle=authoryear, 
  uniquelist=minyear, doi=true, url=false, clearlang=true, annotation=false,
  uniquename=true]{biblatex}
  \usepackage[font={footnotesize}]{caption}
  \usepackage{booktabs}
  \usepackage{makecell}
  \usepackage{dcolumn}
  \usepackage{tocloft}
  \renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
  \usepackage{float}
  \floatplacement{figure}{H}
  \floatstyle{plain}
  \restylefloat{table}
  \newcommand{\beginsupplement}{
    \setcounter{table}{0}
    \renewcommand{\thetable}{S\arabic{table}}
    \setcounter{figure}{0}
    \renewcommand{\thefigure}{S\arabic{figure}}
  }
---


```{r setup, include=FALSE}
library(pacman)
p_load(here)

# Set data path
source(here("working_paper/scripts/support/set_path.R"))

p_load(kableExtra)
options(knitr.kable.NA = "", knitr.table.format = "latex")

p_load(knitr)
knitr::opts_chunk$set(
  fig.width=12, fig.height=7,
  dpi = 300,
  echo=FALSE, warning=FALSE, message=FALSE
  #, fig.path = ""
  )
```

```{r}
source(here("working_paper/scripts/fig_tab_map.R"))
```

\clearpage
\beginsupplement
\renewcommand{\thesection}{\Alph{section}}

# More details on spatial micro-simulation

- Decomposition of household income by occupation - estimation
- map of ETH with adm1 and adm2 units
- Calculation of poverty/SDG 1 targets (expenditure approach)
- page 23 of ETH 2019 pov assessment has a very good explanation on how the poverty line is calculated in ETH and which approach is used.
- Add other common poverty (and food security) indicators that can easily be estimated with out approach (as in povcal)


# Projections




### Subnational population projections




## Benchmark tables and projections


<!-- Add flow chart with procedure on how scenarios were created -->

- Use UN population data for benchmark year (2018)
- Scale SSP age sex population projections, which have base year 2010 to UN age sex base year values. This shifts the projections slightly up as population has increased in comparison to 2010.
- Scale worldpop base year subnat age-sex to national age_sex distribution
- use RAS to scale base year urban-rural map that is consistent with (1) subnational age-sex and (2) Urban-rural national population distribution
- Use SSP urban rural subnational projections to extrapolate adm base year values.
- Use RAS to scale projections to national population projections
- Use SSP national age-sex projections to extrapolate subnational base year age-sex data
- Use RAs to scale projections to subnational urban+rural projections and national age-sex projections


- Use RAS to ensure national total adds up to national population and subnational totals add up to the urban+rural total projections
- Occupation projections.


To create subnational population projections, we start by creating projections for the number of urban and rural population. Our main source of information is @Jones2016b, who use a @urban growth model to prepare global grid-level population projections that are consistent with the national-level SSP population projections. To construct subnational urban, rural and total projections we overlay the grid-level data with the polygon that demarcates the location of the administrative level two units. 

In the next step, we split the total subnational projections in different age and sex classes by using information from the US census bureau (USCB), who prepared detailed subnational baseline projections for a number of countries, including Ethiopia (https://www.census.gov/geographies/mapping-files/time-series/demo/international-programs/subnationalpopulation.html). Unfortunately, we cannot use these projections directly as they only cover the period 2000-2025, only represent one possible (i.e. business-as-usual) future and are based on different assumptions than the SSPs. However, as these projections are based on national population census (2007 is the most recent for Ethiopia), we can use the shares to split the base year (2010) values of the SSP-based subnational total population projections into age and sex classes that are specific for each administrative unit. Age and sex specific projections for the period 2011-2050 are estimated by applying national-level and SSP specific growth rates for each age and sex class. The projections are subsequently 'raked' by means of the iterative proportional fitting method [@ADD] to ensure that (1) the sum over all subnational units and age and sex groups is equal to the national age and sex SSP projections and (2) the sum of the sex and age groups for each subnational unit is the same as the total (urban and rural) subnational population projections.

[@COMPARE uscb and SSP subnational baseline projections]

[Figure that shows examples of projections for two regions - one with biggest rural to urban transformation]


Finally, we use the subnational population projections to estimate the future number of households for each administrative region. Several methods have been proposed to create projections for the number of households [@UN1973; @Mason1992]. Here, we use the headship rate method, which is the most widely used approach [@UN1973; @UK2018PopProj]. The advantage of this approach over others is that it builds upon total population projections by sex and age that are relatively easily available and reflect underlying changes in population composition, which are a determinant of the size and proportion of households. 

The headship method starts with determining age and sex-specific headship rates $h_{as}$, which are the proportion of persons aged $a$ with sex $s$, who head a household:

\begin{equation}
h_{as}= \frac{H_{as}}{P_{as}}
\end{equation}

where $H_{as}$ is the number of heads of households or families by sex $s$, age $a$ and $P_{as}$ the population pf sex $s$ and age $a$. The number of households $\hat{H}_t$in year $t$is then calculated by:

\begin{equation}
\hat{H}_t = \sum_{a}\sum_{s}h_{as}\hat{N}_{ast}
\end{equation}

where $\hat{N}_{ast}$ is the projected population with age $a$ and sex $s$ in year $t$. We used the information from the 2007 population census [@ADD] to calculate $h_{as}$ and apply this to the subnational population projections. As the population census uses a different subnational classification than our other subnational data sources, we estimated the $h_{as}$ at the administrative unit 1 level and apply this to all corresponding level 2 units. We assumed a constant rate of $h_{as}$ for the whole projection period as information on changes in future headship rates are unavailable. Assuming constant headship rates is a frequently used method to construct household projections that use headship rate methods because it relatively simple and can be applied in many settings [@UN1973].

<!-- p33 UN1973 offers and approach to impute number of institutional households
https://development-data-hub-s3-public.s3.amazonaws.com/ddhfiles/160361/jobs-diagnostics-guidelines.pdf offers a database on headship rates and UN1973 argues it can be linked to income per capita and urbanization => perhaps create SSP specific projections-->

 

<!-- ADD equations and explanation using UN1973 and UK document (provides also projection by type of hh). We can either use constant rate or projections of headship rate. As mentioned on p.18 of UN1973, one option would be to regress the headship rate per Woreda or zone on urbanization and use SSP rural/urban shares for the region to project the rate forward.] -->

There are various ways of preparing these projections, simple or complex. For more information:

https://www.ons.gov.uk/peoplepopulationandcommunity/populationandmigration/populationprojections/methodologies/methodologyusedtoproducehouseholdprojectionsforengland2018based


### Age and sex projections

Our main source of information for the benchmark tables is latest Population and Housing Census of Ethiopia, which gives a complete picture of the population for the year 2007 at the Woreda level, the 3rd order administrative division. As the base year for our analysis is 2014 (see below), we use information from the US Census Bureau, who prepares detailed subnational population projections for a large number of countries, including Ethiopia.[^3]. The subnational projections for Ethiopia are based on national population projections prepared by the US Census Bureau, which are disaggregated by using a combination of past trends based on several population census and optimization approaches. Projections are available for a large number of age groups by sex and cover 757 individual Woredas up to the year 2025.

[^3]: <https://www.census.gov/geographies/mapping-files/time-series/demo/international-programs/subnationalpopulation.html>


### Urban-rural projections

To account for the impact of urbanization, we will extend the benchmark tables with spatial information on the proportion of urban and rural population. Our main source of information is @Jones2016b, who present spatially explicit population projections for the period 2000-2050 (see below). The spatial projections have a resolution of \~ 1km x 1km and can be aggregated to the Woreda level.


```{=html}
<!-- Bussolo also adds education level. Shouldn't we include this as this is key for upgrading wage,
similar for occupation -->
```


### Occupation projections

Small area estimations of household income are not part of the national population census and will be an output of the spatial microsimulation. Apart from age, gender and location, income will be strongly determined by occupation and skill of the household member, which might differ across regions. To account for this, we will add subnational information on five major occupational groups from the Ethiopian national labor force survey to the benchmark tables. The classification is based on the ILO occupation classification [@Walmsley2013a]. As this information is only available at the administrative unit 1 level, we assume it is representative for all Woredas located within these regions.


Apart from demographic change, we also need to account for the impact of economic development and structural transformation of the economy on individual and household characteristics. A well-know stylized fact in the economic growth literature is that economic development is accompanied by a process of structural change in which the traditional agricultural sector diminishes while at the same time manufacturing and services expand [@Syrquin1988]. As the economy develops, the demand for high-skilled workers in the manufacturing and services sector increases, inducing inducing people to move out of agriculture, where wages are relatively low. Unfortunately, detailed subnational employment information and projections are not easily available from existing sources. To create the occupation projections, we obtained administrative level 1 employment data from the 2013 Labor Force Survey [@CSA2014]. To project these data forwards, we started by regressing national-level historical data for five occupation types from ILOSTAT: @ADD on GDP per capita. The regression was subsequently combined with SSP GDP per capita projections to generate occupation shares up to 2050. Finally, the growth rates were combined with adm1 subnational occupation data from the labor force survey. 

<!-- [@add part on people not employed, elderly and children similar to hallegate]. Think if we want to apply different employment to population rates of working age per scenario. For SSP we simply use historical adm1 rates. -->
<!-- Add some details on the survey that it is applied to all households and representative at .. and therefore also covers informal employment. -->

<!-- Perhaps take into account sort of downscaling as in Van Vuuren and Gidden -->

Finally, similar to the construction of the base year database, we will use IPF to construct a synthetic population database for 2030.
 level, the 1st order administrative division.



## Household income and poverty projections

<!-- For most developed countries consumption rather then income is used as proxy to assess income and poverty. World Bank PovCal uses Consumption in case of Ethiopia. Probably best to use income projections (or consumption) from MAGNET and apply them to consumption based income (and refer to similarity between income and consumption poverty measures). -->

We used household consumption expenditure as a proxy for income. Consumption-based measure is considered as a much more reliable source than total income for poor countries [@Haughton2009a] and is therefore commonly used as the main indicator for poverty analysis [@WorldBank2020; @Joffe2017]. We followed the guidelines in @Deaton2002 and calculated household income as the sum of annualized expenditures on food, non-food, durables and housing. Per capita income in each household was calculated by diving total household consumption expenditures by household size. National and regional aggregates of per capita income are weighted averages, using the simulated household weights times household size as weights. 

A key indicator to measure progress towards SDG1, listed in the official UN global indicator framework for the Sustainable Development Goals (https://unstats.un.org/sdgs/indicators/indicators-list/) is the _Proportion of the population living below the international poverty line by sex, age, employment status and geographic location (urban/rural)_. As income statistics are only available at household level, we are only able to provide a breakdown by geographic location. The proportion of the population living below the international poverty line is also referred to as the headcount index ($Po$) [@Haughton2009a], defined as:

\begin{equation}
$$P_o = \frac{1}{N}\sum^N_{i=1}I(y_i<z) $$
\end{equation}

where $N$ is the total population, $I(\cdot)$ is an indicator function that takes on a value of 1 if the expression in brackets is true and 0 otherwise, $y_i$ is per capita income and $z$ the poverty line.

The international poverty is currently set at USD\$1.90 per day for extreme poverty and USD\$3.20 per day for for lower middle-income. To compute the poverty lines in Birr for the base year 2018, we followed the standard approach used by the World Bank to harmonize estimates (http://iresearch.worldbank.org/PovcalNet/home.aspx). We used the Ethiopia Purchasing Power Parity (PPP) exchange rates for household consumption from the 2011 International Comparison Program to convert the international poverty lines into 2011 Birr and updated the series to 2018 by applying the domestic consumer price index. Both pieces of information are taken from the World Development Indicators Database [@WorldBank2021]. This resulted in a 2018 poverty line of [@ADD] Birr.


Proportion of population living below the national poverty line, by sex and age
#' Estimation of national poverty line
#' National poverty line in Ethiopia is 7,184 Birr in 2015 (source: ETH poverty assessment, 2020, p 38).
#' To convert this to 2018 values, we need to account for inflation. In ETH, the GDP deflator is used
#' (see box 1 and Annex Figure 2, chapter 1, 2020 pov assessment) but it is more common to use the CPI.
#' Moreover, ETH uses an adult equivalent for hh size to account for differences in consumption between
#' people of different age (See ETH 2019 pov report for Adult equivalent en decompostion method (p 62)
#' and chapter 2. and Table A2.5: Nutritional (calorie) based equivalence scales.

# SDG 1 indicators


-   INDICATOR 1.2.1 Proportion of population living below the national poverty line, by sex and age

-   See povcal for some interesting notes, among others use of regional prices to estimate poverty: <http://iresearch.worldbank.org/PovcalNet/methodology.aspx>

<!-- # Results -->

<!-- Decompose changes over time and between regions using: -->

<!-- - Oxaca-binder decomposition [Ethiopia article, also see Ethiopia poverty assessment, Box 4.1] -->

<!-- - Theil inequality decomposition [Bussolo et al 2012, global inequality analysis] -->

<!-- figuring aid need. -->

<!-- Use hotspot analysis (such as in chapter 5 Tanton (2013)) to identify clusters of people with high/low food security using https://cran.r-project.org/web/packages/rsatscan/index.html -->

<!-- # Discussion -->

<!--  Multi-dimensional  Poverty  Index  (MPI) -->

<!-- ## Missing values -->

<!-- Can use VIMS or MICE to impute missing values in ESS -->

<!-- # Results -->

<!-- - use oxaca-binder decoposition to decompose difference in indicator over time @Bourguignon2008 and @Worku2017 -->

<!-- - use growth incidence curves: https://www.worldbank.org/en/topic/poverty/lac-equity-lab1/economic-growth/growth-incidence-curve to show distributional change over time -->

Proportion of population living below the national poverty line, by sex and age
#' Estimation of national poverty line
#' National poverty line in Ethiopia is 7,184 Birr in 2015 (source: ETH poverty assessment, 2020, p 38).
#' To convert this to 2018 values, we need to account for inflation. In ETH, the GDP deflator is used
#' (see box 1 and Annex Figure 2, chapter 1, 2020 pov assessment) but it is more common to use the CPI.
#' Moreover, ETH uses an adult equivalent for hh size to account for differences in consumption between
#' people of different age (See ETH 2019 pov report for Adult equivalent en decompostion method (p 62)
#' and chapter 2. and Table A2.5: Nutritional (calorie) based equivalence scales.

# Income projections


Total household income is the sum individual household member income, which, in turn, is related to occupation. As a consequence of economic development, the average income for each occupation will change between the base year and 2050. To simulate this, we use a similar approach as @Hallegatte2017 to adjust household income. First, we allocated an activity class to each person in the ESS household survey using the information from the time use and labor module in the questionnaire. We used this to allocate one of the five ILO occupation classes to each person in the household survey.  

We assumed that persons, which indicated that they (1) are engaged with household agricultural activities and did not report an occupation and (2) are currently without a job but aim to return to agricultural work, can be regarded as either self-employed family farm or employed agricultural workers that can be classified as  [@ag_othlowsk]. Apart from the occupation classes, we also created three additional activity classes: elderly (65 years and above), children (under 15 years) and adults that are not working (i.e. the unemployed and not in the labor force such as housewives and students). In order to create a set of mutually exclusive classes, the elderly and children classes precede the ILO occupation classes in the small number where two different classes applied to one person. 

Second, for each household, we count the number of people that belong to each of these classes ($class_i$) and use this to determine the per capita contribution of each class to total household income. 

Third, we combined the coefficients for each activity class income projections from MAGNET to calculate the average income per occupation in 2050. We assumed that income for adults not in the labor force and elderly, which are not simulated in MAGNET, increases at the same rate as [@ag_othlowsk] workers.

It is important to emphasize that the activity classes, and the change in income related to these classes, are used as drivers of income change in our model. We do not assume that they explain all current income or are the only drivers of income change.

As the synthetic population database and the MAGNET use different sources of information and approaches to calculate wage and income, directly mapping macro-growth rates to the household level potentially introduces a bias. To circumvent this, 


- Explain that distribution change depends on combination of (1) weights that account for
demographic change as well as urbanization and occupation change and (2) increase in income for each occupation as economy develops.


Second, for each household, we count the number of people that belong to each of these classes ($class_i$) and use this to determine the per capita contribution of each class to total household income. Following @Hallegatte2017 we estimated the following equation:

\begin{equation}
I_h = \sum_{i=1}^{7}\alpha_{i}class_{ih} + \epsilon_h
(\#eq:eq1)
\end{equation}

where $Ih$ is total income for household $h$ in 2018, $\alpha_i$ are the coefficients for each activity class $i$, $class_{ih}$ are the number of household members with activity class $i$ and $\epsilon$ is the household specific error term. Equation \@ref(eq:eq1) was estimated using weighted linear least squares and without an intercept. We excluded children ($\alpha_9=0$) from the regression as we assumed that they will not generate income in the majority of households. Table \@ref(tab:income-reg) shows the estimated coefficients for each activity class. 

Income per capita can then be expressed as follows:

\begin{equation}
Y_h = \frac{\sum_{i=1}^7(\alpha_{i}\times class_{ih}) + \epsilon_h}{\sum_{i=1}^8class_{ih}}
(\#eq:eq2)
\end{equation}

where $Y_h$ is the average per capita income for household $h$.

(ref:tab-income-reg) **Estimated average per capita contribution to total household income for each activity class.**

```{r tab-income-reg, results="asis"}
texreg(w_ni, 
       caption = "(\\#tab:tab-income-reg) (ref:tab-income-reg)",
       use.packages = FALSE,
       dcolumn = TRUE)
```
<!-- check hallegate annex p9-10 to adjust income for change in food prices and distribution of revenue! -->
Hallegatte2017


A key indicator to measure progress towards SDG1, listed in the official UN global indicator framework for the Sustainable Development Goals (https://unstats.un.org/sdgs/indicators/indicators-list/) is the _Proportion of the population living below the international poverty line by sex, age, employment status and geographic location (urban/rural)_. As income statistics are only available at household level, we are only able to provide a breakdown by geographic location. 


# Validation

## Internal validation

## External valiation

- comparison with small area poverty estimates
- comparison with poverty projections 
National poverty line in Ethiopia is 7,184 Birr in 2015 (source: ETH poverty assessment, 2020, p 38)
Chapter IV provides poverty analysis on the basis of the ESS. Compare our estimates with the HICES and ESS estimates from the world bank and other studies cited in there.
Annex to chapter 4 selects different poverty line!
- Compare adm1 sim and lsms-isa results


# Sensitivity analysis

- BOX 15 2020 eth pov assessment (maybe not useful)
- P215 annex figure 2 - different poverty lines
- Bootstrap of survey data
- occupation income has wide confidence intervals - use 95% interval to assess impact on poverty analysis
- CC scenarios?
- Other parameters in Hallegate
- Different poverty lines
- Adult equivalent versus per capita
- COmpare with povcal estimates


# Drivers of food demand

# Impact of drivers

- kernel density which shows impact of different drivers.
- regress income, urban-rural status on consumption?

# MAGNET projections



# References
