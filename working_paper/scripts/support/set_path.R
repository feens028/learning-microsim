#'=================================================================================================
#' Project:  TEST
#' Subject:  setup path
#' Author:   Michiel van Dijk
#' Contact:  michiel.vandijk@wur.nl
#'=================================================================================================

# Michiel WECR
if(Sys.info()["user"] == "dijk158") {
  proj_path <- "C:/Users/dijk158/Wageningen University & Research/PhD - Ninke - General/micro_data"
  raw_path <- file.path(proj_path, "dutch_fadn/raw_data")
  proc_path <- file.path(proj_path, "data/processed")
}

# Ninke
if(Sys.info()["user"] == "feens028") {
  proj_path <-  "C:/Users/feens028/Wageningen University & Research/PhD - Ninke - General/micro_data"
  raw_path <- file.path(proj_path, "dutch_fadn/raw_data")
  proc_path <- file.path(proj_path, "data/processed")
}

# Anybody else: